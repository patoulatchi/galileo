package galileo.core.rest;

import galileo.core.model.Property;
import galileo.core.service.GenericService;
import galileo.core.model.Id;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by nicolas on 26/09/14.
 */
@RestController
@RequestMapping("/property")
public class PropertyServiceREST extends GenericRESTService<Property, Id>
{
   @Autowired
   private GenericService<Property, Id> service;

   @Override
   public GenericService<Property, Id> getService()
   {
      return service;
   }
}
