package galileo.core.rest;


import galileo.core.exception.StorageException;
import galileo.core.persistency.Identifiable;
import galileo.core.persistency.Storable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

/**
 * Created by nicolas on 26/09/14.
 */
@RestController
public abstract class GenericRESTService<T extends Storable, PK extends Identifiable>
{
   public abstract GenericService<T, PK> getService();

   @RequestMapping(method = RequestMethod.PUT)
   public ResponseEntity<T> create(@RequestBody T t)
   {
      try
      {
         return new ResponseEntity<>(getService().create(t), HttpStatus.CREATED);
      }
      catch (StorageException e)
      {
         e.printStackTrace();
         return new ResponseEntity<>(HttpStatus.ALREADY_REPORTED);
      }
   }

   @RequestMapping(method = RequestMethod.GET)
   public ResponseEntity<Collection<T>> getAll()
   {

      Collection<T> all = getService().getAll();
      return new ResponseEntity<>(all, HttpStatus.OK);
   }


   @RequestMapping(method = RequestMethod.GET, value = "/{galileoId}")
   public ResponseEntity<T> read(@PathVariable PK galileoId)
   {

      T storage = getService().read(galileoId);

      System.out.println("yeah");

      if (storage == null)
      {
         return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      }
      return new ResponseEntity<>(storage, HttpStatus.OK);
   }

   @RequestMapping(method = RequestMethod.DELETE)
   public void delete(PK galileoId) throws StorageException
   {
      getService().delete(galileoId);
   }
}
