package galileo;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by nicolas on 20/09/14.
 */
@ComponentScan
@EnableAutoConfiguration
public class GalileoServer
{

   public static void main(String[] args)
   {
      SpringApplication.run(GalileoServer.class, args);
   }


}
