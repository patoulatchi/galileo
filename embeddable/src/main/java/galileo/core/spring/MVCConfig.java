package galileo.core.spring;

import galileo.core.constants.Constants;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {Constants.REST_CONTROLLER_PACKAGE_NAME})
public class MVCConfig
{
}
