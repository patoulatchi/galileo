package galileo.core.spring;


import galileo.core.constants.Constants;
import galileo.core.config.Config;
import galileo.core.Database;
import galileo.core.persistency.MemoryXMLRepositoryImpl;
import galileo.core.Id;
import galileo.core.exception.StorageException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;

/**
 * Created by nicolas on 25/09/14.
 */

@Configuration
public class SpringBinding
{
   @Autowired
   Config config;

   @Qualifier("XMLFileRepository")
   @Bean
   public MemoryXMLRepositoryImpl<Database, Id> createXmlStorageRepe() throws StorageException
   {
      return new MemoryXMLRepositoryImpl<>(new File (config.getConfigDirectory() + File.separator + Constants.DATABASE_DEFINITION_FILE));
   }

}
