package galileo.core.spring;

import galileo.core.service.initializer.RepositoryInitializer;
import galileo.core.exception.InitializationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import java.util.Set;

/**
 * Created by nicolas on 25/09/14.
 */
public class WebAppInitializer implements WebApplicationInitializer
{
   private static Logger LOG = LoggerFactory.getLogger(WebAppInitializer.class);

   @Autowired
   private RepositoryInitializer initializer;


   @Override
   public void onStartup(ServletContext servletContext) throws ServletException
   {

      WebApplicationContext rootContext = createRootContext(servletContext);

      /*
         Configure Spring MVC
       */
      configureSpringMvc(servletContext, rootContext);

      /*
         Initialize the application
       */
      try
      {
         initializer.initialize();
      }
      catch (InitializationException e)
      {
         throw new ServletException("Cannot initialize the Galileo application", e);
      }


   }

   private WebApplicationContext createRootContext(ServletContext servletContext)
   {
      AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
      rootContext.register(SpringBinding.class);
      rootContext.refresh();

      servletContext.addListener(new ContextLoaderListener(rootContext));
      servletContext.setInitParameter("defaultHtmlEscape", "true");

      return rootContext;
   }

   private void configureSpringMvc(ServletContext servletContext, WebApplicationContext rootContext)
   {
      AnnotationConfigWebApplicationContext mvcContext = new AnnotationConfigWebApplicationContext();
      mvcContext.register(MVCConfig.class);

      mvcContext.setParent(rootContext);

      ServletRegistration.Dynamic appServlet = servletContext.addServlet(
              "webservice", new DispatcherServlet(mvcContext));
      appServlet.setLoadOnStartup(1);
      Set<String> mappingConflicts = appServlet.addMapping("/");

      if (!mappingConflicts.isEmpty())
      {
         for (String s : mappingConflicts)
         {
            LOG.error("Mapping conflict: " + s);
         }
         throw new IllegalStateException(
                 "'webservice' cannot be mapped to '/'");
      }
   }

}
