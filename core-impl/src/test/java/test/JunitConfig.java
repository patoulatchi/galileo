package test;

import galileo.core.config.Config;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Created by nicolas on 05/10/14.
 */
public class JunitConfig implements Config
{

   private static JunitConfig instance;

   public static JunitConfig getInstance()
   {
      if (instance == null)
      {
         instance = new JunitConfig();
      }
      return instance;
   }

   @Override
   public File getConfigDirectory()
   {
      return getFileFromClasspath("config");
   }

   @Override
   public File getDataDirectory()
   {
      return getFileFromClasspath("data");
   }

   private File getFileFromClasspath(String path)
   {
      try
      {
         return ResourceUtils.getFile("classpath:" + path);
      }
      catch (FileNotFoundException e)
      {
         throw new RuntimeException(String.format("The resource %s not found", path), e);
      }
   }
}
