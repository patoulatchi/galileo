package test;

import galileo.core.Database;
import galileo.core.DatabaseRepositoryImpl;
import galileo.core.ServerImpl;
import galileo.core.database.file.FileDatabase;
import galileo.core.exception.StorageException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;

/**
 * Created by nicolas on 25/09/14.
 */
@Configuration
@ContextConfiguration(classes = DatabaseXMLRepoTest.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class DatabaseXMLRepoTest
{
   @Autowired
   private ServerImpl server;

   @Bean
   @Qualifier("StorageRepo")
   DatabaseRepositoryImpl createStorageRepo()
   {
      try
      {
         return new DatabaseRepositoryImpl();
      }
      catch (StorageException e)
      {
         throw new RuntimeException("could not create a XML repository", e);
      }
   }

   @Bean
   ServerImpl getServer()
   {
      return new ServerImpl();
   }

   @Test
   public void test() throws StorageException
   {
      Database mainStorage = server.createDatabase(new FileDatabase("MainStorage", new File("main-database/")));

      mainStorage.createCollection("test");
      mainStorage.createCollection("test2");
      mainStorage.createCollection("test3");

      Database secondaryDatabase = server.createDatabase(new FileDatabase("SecondaryStorage", new File("secondary-database/")));

      secondaryDatabase.createCollection("collection1");
      secondaryDatabase.createCollection("collection2");
      secondaryDatabase.createCollection("collection3");


   }
}
