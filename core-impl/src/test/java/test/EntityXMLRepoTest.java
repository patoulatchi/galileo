package test;

import org.junit.runner.RunWith;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by nicolas on 25/09/14.
 */
@Configuration
@ContextConfiguration(classes = EntityXMLRepoTest.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class EntityXMLRepoTest
{

//   @Autowired
//   @Qualifier("EntityRepo")
//   private MemoryXMLRepositoryImpl<Entity, Id> repository;
//
//   @Bean
//   @Qualifier("EntityRepo")
//   MemoryXMLRepositoryImpl<Entity, Id> createEntityRepo()
//   {
//      try
//      {
//         return new MemoryXMLRepositoryImpl<>(new File(JunitConfig.getInstance().getConfigDirectory().getAbsolutePath() + File.separator + Constants.ENTITY_CONFIG_FILE));
//      }
//      catch (StorageException e)
//      {
//         throw new RuntimeException("could not create a XML repository", e);
//      }
//   }
//
//
//   @Test
//   public void test() throws StorageException
//   {
//
//      Aspect a1 = new Aspect("aspect1");
//      Content c1 = new Content("content1");
//
//      repository.create(a1);
//      repository.create(c1);
//
//      Assert.assertTrue(repository.getAll().size() == 2);
//
//      Entity a1read = repository.read(a1.getId());
//      Assert.assertTrue(a1read.equals(a1));
//
//      Entity c1read = repository.read(c1.getId());
//      Assert.assertTrue(c1read.equals(c1));
//
//      //repository.export(new File(""));
//
//
//   }
}
