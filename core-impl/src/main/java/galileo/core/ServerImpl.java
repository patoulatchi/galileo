package galileo.core;

import galileo.core.exception.StorageException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by nicolas on 08/10/14.
 */
@Service
public class ServerImpl
{
   @Autowired
   DatabaseRepositoryImpl databaseRepository;

   public Database createDatabase(Database database) throws StorageException
   {
      Database database1 = databaseRepository.create(database);
      database1.createPhysicalDatabase();

      return database1;
   }


}
