package galileo.core.persistency;

import galileo.core.constants.Constants;
import galileo.core.exception.StorageException;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created by nicolas on 01/10/14.
 */
@org.springframework.stereotype.Repository
public abstract class FileMemoryRepositoryImpl<T extends Storable, PK extends Identifiable> extends MemoryRepositoryImpl<T, PK> implements FileRepository<T, PK>
{
   private Logger logger = Logger.getLogger(Constants.DEFAULT_LOGGER);
   protected File datafile;

   protected FileMemoryRepositoryImpl()
   {
   }

   protected FileMemoryRepositoryImpl(File datafile) throws StorageException
   {
      this.datafile = datafile;

      if (!datafile.exists())
      {
         logger.warning(String.format("The specified file %s does not exits, it will be automatically created", datafile.getAbsolutePath()));
         try
         {
            if (!datafile.createNewFile())
            {
               throw new StorageException(String.format("The file %s could not be created", datafile.getAbsoluteFile()));
            }
         }
         catch (IOException e)
         {
            throw new StorageException(String.format("The file %s could not be created", datafile.getAbsoluteFile()), e);
         }
      }
      else
      {
         load();
      }
   }

   @Override
   public T create(T t) throws StorageException
   {
      T t1 = super.create(t);
      writeToFile();
      return t1;
   }

   @Override
   public T update(T t) throws StorageException
   {
      T update = super.update(t);
      writeToFile();
      return update;
   }

   @Override
   public void delete(PK id) throws StorageException
   {
      super.delete(id);
      writeToFile();
   }

   @Override
   public void clear() throws StorageException
   {
      super.clear();
      writeToFile();
   }

   public abstract void writeToFile() throws StorageException;

   public abstract void export(File file) throws StorageException;

   public abstract void load() throws StorageException;

}
