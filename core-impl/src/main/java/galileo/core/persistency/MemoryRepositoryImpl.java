package galileo.core.persistency;

import galileo.core.exception.StorageException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by nicolas on 01/10/14.
 */
@org.springframework.stereotype.Repository
public class MemoryRepositoryImpl<T extends Storable, PK extends Identifiable> implements Repository<T, PK>
{

   protected Map<PK, T> map = new HashMap<>();

   @Override
   public T create(T t) throws StorageException
   {
      map.put((PK) t.getId(), t);
      return map.get(t.getId());
   }

   @Override
   public T read(PK id)
   {
      return map.get(id);
   }

   @Override
   public T update(T t) throws StorageException
   {
      map.put((PK) t.getId(), t);
      return map.get(t.getId());
   }

   @Override
   public void delete(PK id) throws StorageException
   {
      map.remove(id);
   }

   @Override
   public void clear() throws StorageException
   {
      map.clear();
   }

   @Override
   public boolean contains(PK id)
   {
      return map.containsKey(id);
   }

   @Override
   public Collection<T> getAll()
   {
      return map.values();
   }

}
