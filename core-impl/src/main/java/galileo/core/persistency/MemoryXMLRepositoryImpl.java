package galileo.core.persistency;


import galileo.core.persistency.jaxb.JaxbMarshaller;
import galileo.core.persistency.jaxb.JaxbUnmarshaller;
import galileo.core.exception.StorageException;

import javax.xml.bind.JAXBException;
import java.io.*;
import java.util.ArrayList;

/**
 * Created by nicolas on 30/09/14.
 */
public class MemoryXMLRepositoryImpl<T extends Storable, PK extends Identifiable> extends FileMemoryRepositoryImpl<T, PK>
{

   public MemoryXMLRepositoryImpl(File datafile) throws StorageException
   {
      super(datafile);
   }

   public void writeToFile() throws StorageException
   {
      export(datafile);
   }

   @Override
   public void export(File file) throws StorageException
   {
      try (FileOutputStream outputStream = new FileOutputStream(file))
      {
         JaxbMarshaller.marshal(new ArrayList<T>(map.values()), outputStream, "wrapper");
      }
      catch (JAXBException e)
      {
         throw new StorageException("Could not write the file", e);
      }
      catch (FileNotFoundException e)
      {
         throw new StorageException("Could not find the specified file " + datafile.getAbsolutePath(), e);
      }
      catch (IOException e)
      {
         throw new StorageException("Could not write the file", e);
      }
   }

   public void load() throws StorageException
   {
      try (FileInputStream inputStream = new FileInputStream(datafile))
      {
         JaxbUnmarshaller.unmarshal(inputStream);
      }
      catch (JAXBException e)
      {
         throw new StorageException("Could not unmarshall the file", e);
      }
      catch (FileNotFoundException e)
      {
         throw new StorageException("Could not find the specified file " + datafile.getAbsolutePath(), e);
      }
      catch (IOException e)
      {
         throw new StorageException("Could not read the file", e);
      }


   }
}
