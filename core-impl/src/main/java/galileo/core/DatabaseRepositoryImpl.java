package galileo.core;

import galileo.core.constants.Constants;
import galileo.core.exception.StorageException;
import galileo.core.persistency.MemoryXMLRepositoryImpl;
import galileo.core.persistency.StorageRepository;
import org.springframework.stereotype.Repository;

import java.io.File;

/**
 * Created by nicolas on 07/10/14.
 */
@Repository
public class DatabaseRepositoryImpl extends MemoryXMLRepositoryImpl<Database, Id> implements StorageRepository
{
   public DatabaseRepositoryImpl() throws StorageException
   {
      super(new File(Constants.DATABASE_DEFINITION_FILE));
   }

   public DatabaseRepositoryImpl(File datafile) throws StorageException
   {
      super(datafile);
   }
}
