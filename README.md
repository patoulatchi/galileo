# README #

## How to open the project in Intellij:

Simply open the file build.gradle

## How to start the embedded webserver

Using the Gradle plugin of Intellij, navigate to the ":core" sub-project and run the target "bootRun". This should start the embedded server residing the class "GalileoServer"

