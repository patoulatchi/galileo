package galileo.core;

import galileo.core.database.file.FileDatabase;
import galileo.core.exception.StorageException;
import galileo.core.persistency.Storable;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicolas on 26/09/14.
 */
@XmlTransient
@XmlAccessorType(value = XmlAccessType.FIELD)
@XmlType(name = "database")
@XmlSeeAlso({
        FileDatabase.class
})
public abstract class Database implements Storable
{
   private Id id;
   private String name;
   private DatabaseType databaseType;
   private DatabaseConfig config;
   private List<Collection> collections = new ArrayList<>();

   public Database()
   {
   }

   public Database(String name, DatabaseType databaseType, DatabaseConfig config)
   {
      this.id = new Id();
      this.name = name;
      this.databaseType = databaseType;
      this.config = config;
   }

   public abstract void createPhysicalDatabase() throws StorageException;

   public abstract Collection createCollection(String name) throws StorageException;

   public Id getId()
   {
      return id;
   }

   public void setId(Id id)
   {
      this.id = id;
   }

   public String getName()
   {
      return name;
   }

   public void setName(String name)
   {
      this.name = name;
   }

   public DatabaseType getDatabaseType()
   {
      return databaseType;
   }

   public void setDatabaseType(DatabaseType databaseType)
   {
      this.databaseType = databaseType;
   }

   public DatabaseConfig getConfig()
   {
      return config;
   }

   public void setConfig(DatabaseConfig config)
   {
      this.config = config;
   }

   protected void addCollection(Collection collection)
   {
      collections.add(collection);
   }
}
