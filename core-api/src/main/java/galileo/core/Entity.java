package galileo.core;

import galileo.core.persistency.Storable;


import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by nicolas on 03/10/14.
 */
@XmlRootElement
public abstract class Entity implements Storable
{
   private Id id;
   private String name;
   private Database database;
   private List<Property> properties;

   protected Entity()
   {
   }

   protected Entity(String name)
   {
      this.name = name;
      this.id = new Id();
   }

   @Override
   public Id getId()
   {
      return id;
   }
}
