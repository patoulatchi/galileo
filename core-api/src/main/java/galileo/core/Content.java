package galileo.core;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by nicolas on 03/10/14.
 */
@XmlRootElement
public class Content extends Entity
{
   private List<Id> referedEntityIds;

   public Content()
   {
   }

   public Content(String name)
   {
      super(name);
   }
}
