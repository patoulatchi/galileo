package galileo.core;

import galileo.core.persistency.Identifiable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.UUID;

/**
 * Created by nicolas on 30/09/14.
 */
@XmlJavaTypeAdapter(Id.class)
public class Id extends XmlAdapter<String, Id> implements Identifiable<String>
{
   private String id;

   public Id()
   {
      id = UUID.randomUUID().toString();
   }

   public Id(String id)
   {
      this.id = id;
   }


   @Override
   @XmlElement
   public java.lang.String getId()
   {
      return id;
   }

   @Override
   public Id unmarshal(String v) throws Exception
   {
      return new Id(v);
   }

   @Override
   public String marshal(Id v) throws Exception
   {
      return v.getId();
   }
}
