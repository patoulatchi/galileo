package galileo.core;

import galileo.core.database.file.FileCollection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "collection")
@XmlSeeAlso({
        FileCollection.class,
})
public abstract class Collection
{
   private String name;

   public Collection()
   {
   }

   public Collection(String name)
   {
      this.name = name;
   }
}
