package galileo.core.exception;

/**
 * Created by nicolas on 28/09/14.
 */
public class CollectionAlreadyExistingException extends StorageException
{
   public CollectionAlreadyExistingException()
   {
   }

   public CollectionAlreadyExistingException(String message)
   {
      super(message);
   }

   public CollectionAlreadyExistingException(String message, Throwable cause)
   {
      super(message, cause);
   }

   public CollectionAlreadyExistingException(Throwable cause)
   {
      super(cause);
   }

   public CollectionAlreadyExistingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
   {
      super(message, cause, enableSuppression, writableStackTrace);
   }
}
