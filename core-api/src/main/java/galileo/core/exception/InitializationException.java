package galileo.core.exception;

/**
 * Created by nicolas on 03/10/14.
 */
public class InitializationException extends Exception
{
   public InitializationException()
   {
   }

   public InitializationException(String message)
   {
      super(message);
   }

   public InitializationException(String message, Throwable cause)
   {
      super(message, cause);
   }

   public InitializationException(Throwable cause)
   {
      super(cause);
   }

   public InitializationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
   {
      super(message, cause, enableSuppression, writableStackTrace);
   }
}
