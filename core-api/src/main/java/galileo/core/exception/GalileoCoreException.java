package galileo.core.exception;

/**
 * Created by nicolas on 28/09/14.
 */
public class GalileoCoreException extends Exception
{
   public GalileoCoreException()
   {
   }

   public GalileoCoreException(String message)
   {
      super(message);
   }

   public GalileoCoreException(String message, Throwable cause)
   {
      super(message, cause);
   }

   public GalileoCoreException(Throwable cause)
   {
      super(cause);
   }

   public GalileoCoreException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
   {
      super(message, cause, enableSuppression, writableStackTrace);
   }
}
