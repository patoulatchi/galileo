package galileo.core.exception;

/**
 * Created by nicolas on 28/09/14.
 */
public class DatabaseAlreadyExistingException extends StorageException
{
   public DatabaseAlreadyExistingException()
   {
   }

   public DatabaseAlreadyExistingException(String message)
   {
      super(message);
   }

   public DatabaseAlreadyExistingException(String message, Throwable cause)
   {
      super(message, cause);
   }

   public DatabaseAlreadyExistingException(Throwable cause)
   {
      super(cause);
   }

   public DatabaseAlreadyExistingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
   {
      super(message, cause, enableSuppression, writableStackTrace);
   }
}
