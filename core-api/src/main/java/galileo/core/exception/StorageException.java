package galileo.core.exception;


/**
 * Created by nicolas on 28/09/14.
 */
public class StorageException extends GalileoCoreException
{
   public StorageException()
   {
   }

   public StorageException(String message)
   {
      super(message);
   }

   public StorageException(String message, Throwable cause)
   {
      super(message, cause);
   }

   public StorageException(Throwable cause)
   {
      super(cause);
   }

   public StorageException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
   {
      super(message, cause, enableSuppression, writableStackTrace);
   }
}
