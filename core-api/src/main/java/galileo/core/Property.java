package galileo.core;

import galileo.core.persistency.Identifiable;
import galileo.core.persistency.Storable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by nicolas on 30/09/14.
 */
@XmlRootElement
public class Property implements Storable
{
   private Id id;
   private String name;
   private Id datatypeId;

   @Override
   public Identifiable getId()
   {
      return id;
   }
}
