package galileo.core.constants;

/**
 * Created by nicolas on 21/09/14.
 */
public class Constants
{

   public final static String NS = "http://com.brasey.schema";

   public static final String ENTITY_REST_PATH = "entity";

   public static final String META_REST_PATH = "meta";

   public static final String COLLECTION_CONFIG_REST_PATH = "/collection-config";
   public static final String STORAGE_REST_PATH = "/storage";

   public static final String REST_CONTROLLER_PACKAGE_NAME = "galileo.core.rest";
   public static final String JAXB_MODEL_PACKAGE_NAME = "galileo.core.model";


   public static final String DEFAULT_LOGGER = "galileo.default";

   public static final String DATABASE_DEFINITION_FILE = "databases.xml";
   public static final String ENTITY_CONFIG_FILE = "entity.xml";
   public static final String PROPERTY_CONFIG_FILE = "property.xml";
   public static final String DATATYPE_CONFIG_FILE = "datatype.xml";


}
