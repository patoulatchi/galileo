package galileo.core;

import galileo.core.database.file.FileConfig;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "database-config")
@XmlSeeAlso({
        FileConfig.class,
})
public abstract class DatabaseConfig
{
}
