package galileo.core.database.file;


import galileo.core.Collection;
import galileo.core.Database;
import galileo.core.DatabaseType;
import galileo.core.constants.Constants;
import galileo.core.exception.StorageException;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created by nicolas on 27/09/14.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "file-database", propOrder = {})
public class FileDatabase extends Database
{


   public FileDatabase()
   {
   }

   public FileDatabase(String name, File dataDirectory)
   {
      super(name, DatabaseType.FileSystem, new FileConfig(dataDirectory));
   }

   private FileConfig getFileConfig()
   {
      return (FileConfig) super.getConfig();
   }

   @Override
   public void createPhysicalDatabase() throws StorageException
   {
      File dataDirectory = getFileConfig().getDataDirectory();
      mkdirs(dataDirectory);
   }

   @Override
   public Collection createCollection(String name) throws StorageException
   {

      File dataFilename = findAvailableDataFile(name);
      if (!dataFilename.exists())
      {
         createFile(dataFilename);
      }
      else
      {
         //TODO: Should we throw an exception here ?
         Logger.getLogger(Constants.DEFAULT_LOGGER).warning(String.format("The physical storage container for storage %s already exists, we are simple using it!", super.getName()));
      }


      FileCollection collection = new FileCollection(name, dataFilename.getName(), FileFormat.XML);
      super.addCollection(collection);

      return collection;
   }

   private void createFile(File file) throws StorageException
   {
      try
      {
         // If the file has been created by another thread, the next call will return false
         if (!file.createNewFile())
         {
            throw new StorageException(String.format("The file %s could not be created!", file.getAbsolutePath()));
         }
      }
      catch (IOException e)
      {
         throw new StorageException(String.format("Could not create the file %s", file.getAbsolutePath()), e);
      }
   }

   private void mkdirs(File file) throws StorageException
   {
      if (!file.exists())
      {
         if (!file.mkdirs())
         {
            throw new StorageException(String.format("The file %s could not be created!", file.getAbsolutePath()));
         }
      }

   }


   private File findAvailableDataFile(String collectionName)
   {
      //TODO: Make sure the name can be use as the file name
      String filenameCandidate = collectionName + ".xml";

      return new File(getFileConfig().getDataDirectory() + File.separator + filenameCandidate);
   }
}

