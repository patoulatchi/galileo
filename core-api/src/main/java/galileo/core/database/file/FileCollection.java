package galileo.core.database.file;

import galileo.core.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "file-collection", propOrder = {
        "fileName",
        "fileFormat"
})
public class FileCollection extends Collection
{
   private String fileName;
   private FileFormat fileFormat;

   public FileCollection()
   {
   }

   public FileCollection(String name, String fileName, FileFormat fileFormat)
   {
      super(name);
      this.fileName = fileName;
      this.fileFormat = fileFormat;
   }




}
