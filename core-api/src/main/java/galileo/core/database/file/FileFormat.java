package galileo.core.database.file;

public enum FileFormat
{
   XML,
   JSON
}
