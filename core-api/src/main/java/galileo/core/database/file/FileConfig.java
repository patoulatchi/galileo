package galileo.core.database.file;

import galileo.core.DatabaseConfig;
import galileo.core.persistency.jaxb.JaxbFileAdapter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.File;

/**
 * Created by nicolas on 08/10/14.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "file-config", propOrder = {
        "dataDirectory"
})
public class FileConfig extends DatabaseConfig
{
   @XmlElement(required = true, type = String.class)
   @XmlJavaTypeAdapter(JaxbFileAdapter.class)
   private File dataDirectory;

   public FileConfig()
   {
   }

   public FileConfig(File dataDirectory)
   {
      this.dataDirectory = dataDirectory;
   }

   public File getDataDirectory()
   {
      return dataDirectory;
   }

   public void setDataDirectory(File dataDirectory)
   {
      this.dataDirectory = dataDirectory;
   }
}
