package galileo.core.persistency;

import galileo.core.exception.StorageException;

import java.util.Collection;

/**
 * Created by nicolas on 01/10/14.
 */
public interface Repository<T extends Storable, PK extends Identifiable>
{
   T create(T t) throws StorageException;

   T read(PK id);

   T update(T t) throws StorageException;

   void delete(PK id) throws StorageException;

   void clear() throws StorageException;

   boolean contains(PK id);

   Collection<T> getAll();
}
