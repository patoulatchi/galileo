package galileo.core.persistency;

import galileo.core.Database;
import galileo.core.Id;

/**
 * Created by nicolas on 07/10/14.
 */
public interface StorageRepository extends FileRepository<Database, Id>
{
}
