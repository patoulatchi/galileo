package galileo.core.persistency;

import galileo.core.Entity;
import galileo.core.Id;

/**
 * Created by nicolas on 07/10/14.
 */
public interface EntityRepository extends Repository<Entity, Id>
{
}
