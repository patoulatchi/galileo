package galileo.core.persistency.jaxb;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.io.File;

public class JaxbFileAdapter
        extends XmlAdapter<String, File>
{


   public File unmarshal(String value)
   {
      return new File(value);
   }

   public String marshal(File value)
   {
      return value.getAbsolutePath();
   }

}
