package galileo.core.persistency.jaxb;

import javax.xml.bind.annotation.XmlAnyElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nicolas on 30/09/14.
 */
public class Wrapper<T>

{
   private List<T> items;

   public Wrapper()
   {
      items = new ArrayList<T>();
   }

   public Wrapper(List<T> items)
   {
      this.items = items;
   }

   @XmlAnyElement(lax = true)
   public List<T> getItems()
   {
      return items;
   }
}
