package galileo.core.persistency.jaxb;

import galileo.core.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

/**
 * Created by nicolas on 30/09/14.
 */
public class JaxbWrapper
{
   public static JAXBContext getJaxbContext() throws JAXBException
   {
      return JAXBContext.newInstance(Wrapper.class, Database.class, Aspect.class, Content.class, DataType.class, Entity.class, Item.class, PrimitiveDataType.class, Property.class, ReferenceDataType.class, Id.class);

   }
}
