package galileo.core.persistency.jaxb;

import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamSource;
import java.io.InputStream;
import java.util.List;

/**
 * Created by nicolas on 30/09/14.
 */
public class JaxbUnmarshaller
{
   public static <T> List<T> unmarshal(InputStream is) throws JAXBException
   {

      StreamSource xml = new StreamSource(is);
      Wrapper<T> wrapper = (Wrapper<T>) JaxbWrapper.getJaxbContext().createUnmarshaller().unmarshal(xml,
              Wrapper.class).getValue();
      return wrapper.getItems();
   }
}
