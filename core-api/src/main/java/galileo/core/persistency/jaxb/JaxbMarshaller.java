package galileo.core.persistency.jaxb;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import java.io.OutputStream;
import java.util.List;

/**
 * Created by nicolas on 30/09/14.
 */
public class JaxbMarshaller
{
   public static void marshal(List<?> list, OutputStream os, String name)
           throws JAXBException
   {

      QName qName = new QName(name);
      Wrapper wrapper = new Wrapper(list);
      JAXBElement<Wrapper> jaxbElement = new JAXBElement<Wrapper>(qName,
              Wrapper.class, wrapper);
      Marshaller marshaller = JaxbWrapper.getJaxbContext().createMarshaller();
      marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

      marshaller.marshal(jaxbElement, os);
   }
}
