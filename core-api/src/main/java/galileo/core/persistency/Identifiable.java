package galileo.core.persistency;

import java.io.Serializable;

/**
 * Created by nicolas on 30/09/14.
 */
public interface Identifiable<T extends Serializable>
{
   T getId();
}
