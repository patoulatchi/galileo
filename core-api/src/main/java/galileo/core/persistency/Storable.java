package galileo.core.persistency;

/**
 * Created by nicolas on 30/09/14.
 */
public interface Storable
{
   Identifiable getId();
}
