package galileo.core.persistency;

import galileo.core.exception.StorageException;

/**
 * Created by nicolas on 03/10/14.
 */
public interface FileRepository<T extends Storable, PK extends Identifiable> extends Repository<T, PK>
{
   void writeToFile() throws StorageException;

   void load() throws StorageException;
}
