package galileo.core;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by nicolas on 28/09/14.
 */
@XmlRootElement
public enum DatabaseType
{
   FileSystem,
   MongoDB
}